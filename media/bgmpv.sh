#!/usr/bin/env bash

xwinwrap -ov \
    -st \
    -s \
    -nf \
    -b \
    -un \
    -argb \
    -fs \
    -fdt \
    -o 1.0 \
    -d \
    -debug \
    -- mpv "$1" \
        -wid WID \
        --no-osc \
        --no-osd-bar \
        --player-operation-mode=cplayer \
        --panscan=1.0
