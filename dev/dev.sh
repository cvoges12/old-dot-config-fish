#!/usr/bin/env bash
alias project="~/.config/fish/dev/project.sh"
alias clean="~/.config/fish/dev/clean.sh 2> /dev/null"
