abbr cache 'cd $XDG_CACHE_HOME'
abbr config 'cd $XDG_CONFIG_HOME'
abbr share 'cd $XDG_DATA_HOME'
abbr data 'cd $XDG_DATA_HOME'
abbr dev 'cd $HOME/Dev/'
abbr tmp 'cd /tmp'
abbr mktmp 'mkdir /tmp/'

alias b=$BROWSER
alias v=$EDITOR

alias bookmarks='v Media/Docs/misc/bookmarks.md'

alias sbed="$XDG_CONFIG_HOME"/screenlayout/bed.sh
alias smobile="$XDG_CONFIG_HOME"/screenlayout/mobile.sh
alias soffice="$XDG_CONFIG_HOME"/screenlayout/office.sh

alias gpg="gpg2 --homedir $GNUPGHOME "
alias minecraft="flatpak run com.mojang.Minecraft"
alias ssh="ssh -F $SSH_CONFIG -i $SSH_ID "
alias ssh-copy-id="ssh-copy-id -i $SSH_ID "
alias wal='$XDG_CONFIG_HOME/fish/wal.sh'
alias xinit="xinit $XINITRC"
alias xrdb="xrdb -load $XRESOURCES "

alias init-cv='git init; git config user.email "vogesclayton@gmail.com"; git config user.name "Clayton Voges"'
alias init-midi='git init; git config user.email "storm89161@gmail.com"; git config user.name "JustMidi"'
alias first-commit='git add .; git commit -m "First commit"'
