#!/usr/bin/env bash
alias install='xbps-install -Sy'
alias installed='xbps-query -m'
alias remove='xbps-remove -Ry'
alias search='xbps-query -Rs'
alias update='xbps-install -Syu'
