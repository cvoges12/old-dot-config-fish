#!/usr/bin/env bash

alias bgmpv="~/.config/fish/media/bgmpv.sh"

alias mbsync="mbsync -c ~/.config/isync/mbsyncrc -a"

#alias yt="~/.config/fish/media/yt.sh"
#alias dyt="~/.config/fish/media/dyt.sh"
#alias yts   # search
#alias dytp  # download playlist
#alias ayt   # audio
#alias adyt  # audio download
#alias adytp # audio download playlist
#alias vyt   # video
#alias vdyt  # video download
#alias vdytp # video download playlist
