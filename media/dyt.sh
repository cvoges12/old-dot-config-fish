#!/usr/bin/env bash
case $1 in
    *youtube* | *invid*)
        v=$(echo $1 | sed -e "s/.*watch?v\=//")
        ;;
    *youtu\.be*)
        v=$(echo $1 | sed -e "s/.*youtu.be\///")
        ;;
    *)
        v="$1"
        ;;
esac
youtube-dl -f best "https://youtube.com/watch?v=$v" -o "~/Media/Videos/%(uploader)s --- %(title)s.%(ext)s"
