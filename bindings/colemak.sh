#!/usr/bin/env bash

# Motion
bind h backward-char
bind -M visual h backward-char
bind n down-or-search
bind -M visual n down-line
bind e up-or-search
bind -M visual e up-line
bind i forward-char
bind -M visual i forward-char

bind l backward-word
bind -M visual l backward-word
bind L backward-bigword
bind -M visual L backward-bigword
bind y forward-word forward-char
bind -M visual y forward-word
bind Y forward-bigword forward-char
bind -M visual Y forward-bigword
bind u forward-char forward-word backward-char
bind -M visual u forward-wor
bind u forward-bigword backward-char
bind -M visual U forward-bigword

# Modes
bind -m insert s repaint-mode
bind -m replace_one r repaint-mode
bind -m replace R repaint-mode
bind -m visual v begin-selection repaint-mode

# Actions
bind -M visual -m default c kill-selection yank end-selection repaint-mode
bind -M visual -m insert w kill-selection end-selection repaint-mode
bind -M visual -m default d kill-selection end-selection repaint-mode
bind p yank
bind z history-search-backward
