#!/usr/bin/env bash
rm ~/.bash*
rm ~/.fehbg
rm ~/.lesshst
rm ~/.python*
rm ~/.Xauthority
rm ~/*png
rm ~/Desktop -rf
rm ~/Downloads/* -rf
rm ~/.screenlayout -rf
rm ~/.node_repl_history
rm ~/.wget-hsts
