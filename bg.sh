#!/usr/bin/env bash

xwinwrap -ov -st -ni -s -nf -b -un -argb -fs -fdt -o 1.0 -debug -- mpv "$1" -wid WID --no-osc --no-osd-bar --player-operation-mode=cplayer --panscan=1.0 --no-input-default-bindings
