#!/usr/bin/env bash
alias wttr="curl 'https://wttr.in/?m&M&F&A&Q'" 
alias doppler='mpv --loop-file=inf "https://radar.weather.gov/lite/N0R/LSX_loop.gif"'
